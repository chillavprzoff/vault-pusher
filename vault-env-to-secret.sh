#!/bin/bash

if [[ $VAULTP_SECRET_ENGINE_NAME == "" ]]; then
  VAULTP_SECRET_ENGINE_NAME="secret"
fi

if [[ $VAULTP_ENV_FILE_PATH == "" ]]; then
  ENV=($(cat $1 | sed '/^[[:space:]]*$/d' | sed '/^$/d' | sed "s/#.*//g" | sed ':a;N;$!ba;s/\n/ /g' ))
else
  ENV=($(cat $VAULTP_ENV_FILE_PATH | sed '/^[[:space:]]*$/d' | sed '/^$/d' | sed "s/#.*//g" | sed ':a;N;$!ba;s/\n/ /g' ))
fi

PAYLOAD='{ "data": {'
i=0
for e in ${ENV[@]} ; do
  i=$(($i + 1))
  if [[ $e != "" ]] || [[ $e != "\n" ]] || ! echo $e | grep -q "#"; then
    if [[ $i != ${#ENV[@]}  ]]; then
      KEY_VALUE='"'$(echo $e | awk -F '=' '{print $1}' )'": "'$(echo $e | cut -f 2- -d"=" | sed 's/"//g')'",'
    else
      KEY_VALUE='"'$(echo $e | awk -F '=' '{print $1}' )'": "'$(echo $e | cut -f 2- -d"=" | sed 's/"//g')'"'
    fi
    PAYLOAD=''$PAYLOAD' '$KEY_VALUE''
  fi
done
PAYLOAD="$PAYLOAD } }"

if ! echo $PAYLOAD | jq ".data"; then
  echo ""
  echo "payload not valid!"
  echo ""
  echo $PAYLOAD
else
  echo $PAYLOAD > vault-payload.json
  curl --request POST \
    --url $VAULTP_ADDR/v1/$VAULTP_SECRET_ENGINE_NAME/data/$VAULTP_SECRET_PATH \
    --header 'Content-Type: application/json' \
    --header 'X-Vault-Token: '$VAULTP_VAULT_TOKEN'' \
    --data @vault-payload.json
fi

if [[ -f vault-payload.json ]]; then
  rm -rf vault-payload.json
fi
