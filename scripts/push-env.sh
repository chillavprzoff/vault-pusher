#!/bin/bash

VAULT_ADDR=""
VAULT_SECRET_PATH=""
VAULT_TOKEN=""

backend=$(ls)
for b in ${backend[@]}; do
  if [[ -d $b ]]; then
	docker run -it --rm \
	    -e VAULTP_ADDR=$VAULT_ADDR \
	    -e VAULTP_SECRET_PATH=$VAULT_SECRET_PATH/$b \
	    -e VAULTP_VAULT_TOKEN=$VAULT_TOKEN \
	    -e VAULTP_ENV_FILE_PATH=/script/.env \
	    -v $(pwd)/$b/.env:/script/.env \
	    registry.gitlab.com/chillavprzoff/vault-pusher
  fi
  echo ""
done
