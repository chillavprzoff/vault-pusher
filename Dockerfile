FROM bash
WORKDIR /script
COPY ./vault-env-to-secret.sh .

RUN apk add jq
RUN apk add curl

CMD ["bash", "vault-env-to-secret.sh"]
