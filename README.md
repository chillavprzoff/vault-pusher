# Vault Pusher

Use for upload env or secret to particular vault secret

## GLOBAL ENVIRONMENT VARIABLE

| Environment Name | Default Value | Type | Description | Mandatory |
|---|---|---|---|---|
| VAULTP_ADDR  | null | String | Vault address/url | true |
| VAULTP_VAULT_TOKEN  | null | String | Vault token | true |
| VAULTP_SECRET_ENGINE_NAME | secret | String | Name of vault secret engine, default to 'secret' | |
| VAULTP_SECRET_PATH | null | String | Vault key value secret path | true |
| VAULTP_ENV_FILE_PATH | null | String | Path of the env file | true |

## Examples

### Bash
```bash
VAULTP_ADDR=http://vault.example.com \
    VAULTP_SECRET_PATH=env/example-app \
    VAULTP_VAULT_TOKEN=exampletoken \
    bash vault-env-to-secret.sh /path/to/env/file
```

### Docker
> docker workdir /script

```bash
docker run -it --rm \
    -e VAULTP_ADDR=http://vault.example.com \
    -e VAULTP_SECRET_PATH=env/example-app \
    -e VAULTP_VAULT_TOKEN=exampletoken \
    -e VAULTP_ENV_FILE_PATH=/script/file \
    -v /path/to/env/file:/script/file \
    registry-gitlab.festiware.com/yusril.mohammad/vault-pusher

```
## More details
- `/path/to/env/file`, 'file' is your actual filename, example if your file name is `example-app.env` so the path is `/path/to/env/example-app.env`
- same if you use docker the environment `VAULTP_ENV_FILE_PATH` value must be `/script/example-app.env` and the volume must be the same aswell, like this:
```
-v /path/to/env/example-app.env:/script/example-app.env
```
